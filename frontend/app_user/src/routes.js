import React from 'react'
import UserList from './components/user/list'
import UserCreate from './components/user/create'
import Login from './components/login/login'
import {
    BrowserRouter as Router,
    Switch,
    Route
    // Redirect
} from "react-router-dom";


const Routers = () => (
    <Router>
        <Switch>
            <Route exact path="/list" component={UserList} />
            <Route exact path="/create" component={UserCreate} />
            <Route exact path="/edit/:id" component={UserCreate} />
            <Route exact path="/" component={Login} />
            {/* <Route exact path="*" component={() => (<Redirect to="/" />)} /> */}
            <Route exact path="*" component={() => (<h1>404 | Not Found</h1>)} />

        </Switch>
    </Router>
)

export default Routers;

