import { clientHttp } from '../config/config.js'

const createUser = (data) => clientHttp.post(`/`, data)

const updateUser = (data) => clientHttp.put(`/${data._id}`, data)
// TODO: Verificar no back  a atualização

const ListUser = () => clientHttp.get(`/`)

const DeleteUser = (email) => clientHttp.delete(`/${email}`)

const showUserId = (id) => clientHttp.patch(`/${id}`)

export {
    createUser,
    ListUser,
    DeleteUser,
    showUserId,
    updateUser
}