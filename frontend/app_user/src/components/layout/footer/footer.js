import React from 'react'
import './footer.css'
import imgLogo from '../../../assets/images/logo_organic.png'

const Footer = () => (
    <footer>
        <div className="projectName">
            Organic Food - Mais perto do seu cliente
        </div>
        <div className="copyRight">
            <img src={imgLogo} alt="" />
        </div>
    </footer>
)

export default Footer