import React from 'react'
import './nav.css'
import { useHistory } from 'react-router-dom';

const Nav = (props) => {

    const history = useHistory()

    const changePage = () => history.push(props.to)

    return (
        <nav className="">
            <div className="title"> Lista de Usuários</div>
            <div className="action">
                <button onClick={changePage}>
                    {props.name}
                </button>
            </div>
        </nav>
    )
}
export default Nav;