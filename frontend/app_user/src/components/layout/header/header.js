import React from 'react'
import imgLogo from '../../../assets/images/logo_organic.png'
import './header.css'

const Header = (props) => {
    return (
        <header>
            <div className="title">{props.title}</div>
            <div className="logo">
                <img src={imgLogo} alt="Organic Food" />
            </div>
        </header>
    )
}

export default Header