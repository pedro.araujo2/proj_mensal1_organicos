import React, { useState } from 'react'
import { createUser } from '../../services/user'
import Loading from '../loading/loading'
import Nav from '../layout/nav/nav'
import './user.css'
import { useHistory } from 'react-router-dom'

const UserCreate = (props) => {

    const [isSubmit, setIsSubmit] = useState(false)
    
    const [form, setForm] = useState({
        
    })

    const history = useHistory()

    const handleChange = (event) => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        })
        return;
    }

    const formIsValid = () => {
        return form.social && form.cnpj && form.endereco && form.cep && form.cidade && form.uf && form.telefone && form.web && form.email && form.senha
    }

    const submitForm = async (event) => {
        try {
            setIsSubmit(true)
            await createUser(form)
            alert('Seu formulário foi enviado com sucesso')
            setForm(() => props.mudaPagina('/list'))

            setIsSubmit(false)
            props.mudaPagina('/list')
        } catch (error) {
            console.log('deu ruim')
        }
    }
    return (

        <React.Fragment>
            <Nav name="Lista" to="/list" />
        <section >
            

            <form id="form_cadastro">
                <div>
                    <fieldset className="input-block">
                        <legend>Sua empresa</legend>

                        <div>

                            <input disabled={isSubmit} type="text" name="social" id="social" onChange={handleChange} value={form.social || ""} placeholder="Razão Social" />
                        </div>

                        <div>

                            <input disabled={isSubmit} type="text" name="cnpj" id="cnpj" onChange={handleChange} value={form.cnpj || ""} placeholder="CNPJ" />
                        </div>

                    </fieldset>

                    <fieldset className="input-block">

                        <legend>Onde você está?</legend>

                        <div>
                            <input disabled={isSubmit} type="text" name="endereco" id="endereco" onChange={handleChange} value={form.endereco || ""} placeholder="Endereço" />
                        </div>

                        <div>
                            <input disabled={isSubmit} type="text" name="cep" id="cep" min="8" max="8" onChange={handleChange} value={form.cep || ""} placeholder="CEP" />
                        </div>

                        <div>
                            <input disabled={isSubmit} type="text" name="cidade" id="cidade" onChange={handleChange} value={form.cidade || ""} placeholder="Cidade" />
                        </div>

                        <div>
                            <label htmlFor="uf">UF:</label>
                            <select disabled={isSubmit} name="uf" id="uf" onChange={handleChange} value={form.uf || ""} placeholder="UF">
                                <option value="">Selecione</option>
                                <option value="AC">AC</option>
                                <option value="AL">AL</option>
                                <option value="AP">AP</option>
                                <option value="AM">AM</option>
                                <option value="BA">BA</option>
                                <option value="CE">CE</option>
                                <option value="DF">DF</option>
                                <option value="ES">ES</option>
                                <option value="GO">GO</option>
                                <option value="MA">MA</option>
                                <option value="MS">MS</option>
                                <option value="MT">MT</option>
                                <option value="MG">MG</option>
                                <option value="PA">PA</option>
                                <option value="PB">PB</option>
                                <option value="PR">PR</option>
                                <option value="PE">PE</option>
                                <option value="PI">PI</option>
                                <option value="RJ">RJ</option>
                                <option value="RN">RN</option>
                                <option value="RS">RS</option>
                                <option value="RO">RO</option>
                                <option value="RR">RR</option>
                                <option value="SC">SC</option>
                                <option value="SP">SP</option>
                                <option value="SE">SE</option>
                                <option value="TO">TO</option>
                            </select>
                        </div>

                    </fieldset>

                    <fieldset className="input-block">
                        <legend>Detalhes de contato</legend>

                        <div>
                            <input disabled={isSubmit} type="tel" name="telefone" id="telefone" min="11" max="11" onChange={handleChange} value={form.telefone || ""} placeholder="Telefone" />
                        </div>

                        <div>
                            <input disabled={isSubmit} type="url" name="web" id="web" onChange={handleChange} value={form.web || ""} placeholder="URL" />
                        </div>

                        <div>
                            <input disabled={isSubmit} type="email" name="email" id="email" onChange={handleChange} value={form.email || ""} placeholder="Email" />
                        </div>

                        <div>
                            <input disabled={isSubmit} type="password" name="senha" id="senha" onChange={handleChange} value={form.senha || ""}placeholder="Insira sua senha" />
                        </div>


                    </fieldset>
                    
                    <button disabled={!formIsValid()} onClick={submitForm}>Cadastrar</button>
                </div>
                <Loading show={isSubmit}/>
                {isSubmit ? <div>Carregando....</div> : ""}                    
            </form>            
        </section >
        </React.Fragment>
        
    )
}

export default UserCreate

