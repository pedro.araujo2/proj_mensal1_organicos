import React, { useEffect, useState } from 'react'
import { ListUser, DeleteUser } from '../../services/user'
import Loading from '../loading/loading'
import Nav from '../../components/layout/nav/nav'
import { useHistory } from 'react-router-dom'

const UserList = () => {
    const [users, setUsers] = useState([])
    const history = useHistory()
    const getList = async () => {
        try {
            const users = await ListUser()
            setUsers(users.data)
        } catch (error) {
            console.log('error', error)
        }
    }

    const editUser = (user) => history.push(`/edit/${user._id}`)

    const deleteUser = async ({email, social}) => {
        try {
            if (window.confirm(`Você deseja excluir a empresa ${social}`)){
                await DeleteUser(email)
                getList()
            }
        }catch (error) {
            console.log(error)
        }
    }
    
    const verifyIsEmpty = users.length === 0


    const montarTabela = () => {

    const linhas = users.map((user, index) => (
    <tr key={index}>
      <td>{user.social}</td>
      <th>{user.cnpj}</th>
      <td>{user.endereco}</td>
      <td>{user.cep}</td>
      <td>{user.cidade}</td>
      <td>{user.uf}</td>
      <td>{user.telefone}</td>
      <td>{user.web}</td>
      <td>{user.email}</td>
      <td>
        <span onClick={() => editUser(user)}> Editar </span> |
        <span onClick={() => deleteUser(user)}> Excluir </span>
      </td>
    </tr>
  ))

  return !verifyIsEmpty ? (
    <table>
            <thead>
                <tr>
                    <th>RAZÃO SOCIAL</th>
                    <th>CNPJ</th>
                    <th>ENDEREÇO</th>
                    <th>CEP</th>
                    <th>CIDADE</th>
                    <th>UF</th>
                    <th>TELEFONE</th>
                    <th>WEB</th>
                    <th>EMAIL</th>
                    <th>AÇÕES</th>                    
                </tr>
            </thead>            
            <tbody>
            {linhas}
          </tbody>
        </table>     
        ) : ""
    }
    useEffect(() => {
        getList()
    }, [])


    //render
    return (
        <div>
            <Nav name="Novo" to="/create" />
        <section>
            <div className="list_user">
                <Loading show={verifyIsEmpty} />
                {montarTabela()}
            </div>
        </section>
        </div>
    )
}

export default UserList