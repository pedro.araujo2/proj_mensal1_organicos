#bootcamp-fullStack-proj_ind1-orgânicos

Primeiro projeto individual do Bootcamp FullStack Infnet.

## Versão

0.1.0 (beta)

## Pre requisitos
* node 

## Instalação
`` npm install ```

## Root API

/api/

## TODO 

* api POST user

* api GET USER

* api GET user/:id

* api PUT user

* api PATCH user

* api DELETE user

* config MONGODB