const express = require('express')
var bodyParser = require('body-parser')
const connectDB = require('./config/db');
var cors = require('cors')
const app = express()
const PORT = 3001

app.use(cors())
app.use(express.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

// Connect Database
connectDB()

app.use ('/', require('./routes/api/user'))




app.listen(PORT, () => { console.log(`Conectado à http://localhost:${PORT}`) })
